simple-telnet
=============

## Go get

```bash
go get -v gitlab.com/tyabuta/go-simple-telnet
```


## Install commands

```bash
go get -v -u gitlab.com/tyabuta/go-simple-telnet/cmd/simple-telnet
```


## Usage

### simple-telnet --help

```
NAME:
   simple-telnet - Simple telnet server and client.

USAGE:
   simple-telnet [options]

VERSION:
   0.1.0

AUTHOR:
   tyabuta <gmforip@gmail.com>

OPTIONS:
   --host value, -H value  Connect to Host. (default: "localhost")
   --port value, -P value  Connect or Listen to Port. (default: 5555)
   --server, -s            Server Mode.
   --help, -h              show help
   --version, -v           print the version
```

## Server Mode

```
simple-telnet -s -P 5555
```

## Client Mode

```
simple-telnet -H localhost -P 5555
```


